Just some notes, stored on GitHub instead of a blog.

I am not a native English speaker, so please excuse any language mistakes.

Contents
--
 * [Do not underestimate credentials leaks](https://github.com/ChALkeR/notes/blob/master/Do-not-underestimate-credentials-leaks.md) (2015-12-04)
